<head>
    <title>DiliTrustTestTech</title>
    <link rel="icon" href="https://t3i.fr/DiliTrust/dilitrustIcon.png"/>
</head>

<body>

<a href="https://gitlab.com/t3ick/testtechdilitrust">
    <img src="gitlabLogo.png" width="20" height="20">
    The DiliTrust CustomWebSite Public Gitlab Repo Here
</a>

<div>
    <input type="checkbox" id="vue" name="vue" checked>
    <label for="vue">init vue.js</label>
    <a href="http://t3i.fr:8080/">
        front vue3
    </a>
</div>

<div>
    <input type="checkbox" id="symfony" name="symfony" checked>
    <label for="symfony">init symfony</label>
    <a href="https://t3i.fr/DiliTrust/api/public/">
        api symfony5
    </a>
</div>

<div>
    <input type="checkbox" id="bdd" name="bdd" checked>
    <label for="bdd">link bdd</label>
</div>

<div>
    <input type="checkbox" id="api" name="api">
    <label for="api">install api platform</label>
    <a href="https://t3i.fr/DiliTrust/api/public/api">
        api platform
    </a>
</div>

</body>


<?php